#!/usr/bin/bash
# script by Seth Moeckel 02/27/17

# this script removes all files not relevant to execution
# from a compiled VerneMQ plugin and creates a tar archive
# for storage or transfer.

if [ -d "_build/default" ]; then
    echo "===> Performing Plugin Cleanup!"
	echo "--> Removing all non .app or .beam files"
	find _build/default -not -name "*.app" -type f -not -name "*.beam" -type f -delete

	echo "--> Removing empty dirs..."
	find _build/default -type d | xargs rmdir > /dev/null 2>&1

	echo "--> Compressing plugin..."
	tar -czvf vmq_auth_chx_plugin.tar.gz _build/default
	echo "Done."
else 
	echo "Directory '_build/default' could not be found!"
fi
