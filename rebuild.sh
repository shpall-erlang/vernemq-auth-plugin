#!/usr/bin/env bash
echo "======================== Rebuilding plugin ========================"
cd dev && ./rebuild-plugin.sh && cd ..
echo "========================= Plugin rebuilt =========================="
echo "======================== Rebuilding image  ========================"
rm -rf vmq_auth_chx/_build && cp -r ./dev/_build vmq_auth_chx/_build && docker build --no-cache --pull -t vmq:localdev .
echo "========================= Image rebuilt  =========================="
