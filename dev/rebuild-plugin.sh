#!/usr/bin/env bash

rm -rf sources _build

cp -r ../vmq_auth_chx sources

rm -rf sources/_build sources/rebar.lock

docker build --no-cache --pull -t vmq-plugin-dev .

container_id=$(docker create vmq-plugin-dev)

echo "container_id $container_id"

docker cp ${container_id}:/vernemq-auth-plugin/_build _build

docker container rm ${container_id}

docker rmi vmq-plugin-dev

rm -rf sources
