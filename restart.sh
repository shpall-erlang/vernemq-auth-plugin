#!/usr/bin/env bash
./rebuild.sh && docker container prune -f
echo "============================= Starting ============================="
docker run -it --rm -p1883:1883 --name vmq \
    -e DOCKER_VERNEMQ_plugins__vmq_auth_chx="on" \
    -e DOCKER_VERNEMQ_plugins__vmq_auth_chx__path="/plugins" \
    -e DOCKER_VERNEMQ_plugins__vmq_auth_chx__priority="5" \
    -v ${PWD}/vmq.passwd:/etc/vernemq/vmq.passwd \
    -v ${PWD}/vmq.acl:/etc/vernemq/vmq.acl \
    vmq:localdev \
    start_vernemq -env VMQ_AUTH_CHX_SECRET default-test-secret
