# Example to show PoC JWT-usage with VerneMQ

Next example shows how to test JWT plugin. Using default host (localhost), default port (1883, without TLS).


## Prerequisite
- Any OS **except Windows**
- git
- docker
- Erlang/Rebar3

Binaries must be in `PATH` 


## Prepare

```
git clone git@gitlab.com:shpall-erlang/vernemq-auth-plugin.git
cd vernemq-auth-plugin
./restart.sh
```

[`./restart.sh`](./restart.sh) is all what you need to do. This script will:
- call [`rebuild.sh`](rebuild.sh), which will:
    - rebuild current plugin using docker (see [`dev`](dev) directory, [`dev/rebuild-plugin.sh`](dev/rebuild-plugin.sh) which uses [`dev/Dockerfile`](dev/Dockerfile))
    - put binaries туда, куда-надо
    - build docker-image named `vmq:localdev` (see [`Dockerfile`](`Dockerfile`), which only get official [VerneMQ](https://hub.docker.com/r/erlio/docker-vernemq/) image and put there current plugin)
- start built `vmq:localdev` image with docker using sample configuration

TADAAA, now you can test.


## Testing

Subscribe to all topics as **nifi** user (user named **nifi** can only read all topics):
```
mosquitto_sub -d -i nifi -u nifi -P nifi -t "#"
```

In another terminal publish test message (user named **test** can pub/sub anywhere):
```
mosquitto_pub -i test -u test -P test -t some/test/topic -m SomeTestMessage
```

Check, that everything is OK (you can see `SomeTestMessage` in subscriber terminal).

Next, lets check basic auth (specify wrong password):
```
mosquitto_pub -i test -u test -P wrongpass -t some/test/topic -m SomeTestMessage
```
You should see something about:
```
Connection Refused: bad user name or password.
Error: The connection was refused.
```

Next step is to test JWT-authentication.
You can go to: https://jwt.io/ to generate some test token.
You should specify:
- `"userId"` in `PAYLOAD`, value - the `clientId` of client (set to `client1gameA` for example).
- Secret (set to default value: `default-test-secret`).

Example token here: https://jwt.io/#debugger-io?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjbGllbnQxZ2FtZUEifQ.R2b_RM8LGMrngXbfGarwWV6rH_7dctG3xA8qshgT4g8

Now, lets test that:
```
mosquitto_pub -m TestMsgFromClnt1GameA -u JWT -t client/gameA/client1gameA -i client1gameA -P eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjbGllbnQxZ2FtZUEifQ.R2b_RM8LGMrngXbfGarwWV6rH_7dctG3xA8qshgT4g8
mosquitto_pub -m TestMsgFromClnt1GameA-EVENT1 -u JWT -t client/gameA/client1gameA/event1 -i client1gameA -P eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjbGllbnQxZ2FtZUEifQ.R2b_RM8LGMrngXbfGarwWV6rH_7dctG3xA8qshgT4g8
```
We sent two messages to topics:
- `client/gameA/<clientId>`
- `client/gameA/<clientId>/<someEvent>`

In subscribers terminal You should see something about:
```
Client nifi received PUBLISH (d0, q0, r0, m0, 'client/gameA/client1gameA', ... (21 bytes))
TestMsgFromClnt1GameA
Client nifi received PUBLISH (d0, q0, r0, m0, 'client/gameA/client1gameA/event1', ... (28 bytes))
TestMsgFromClnt1GameA-EVENT1
```


## How it works

First of all - official documentation about VerneMQ plugins:
- [Overview](https://docs.vernemq.com/configuring-vernemq/plugins)
- [Introduction to plugin development](https://docs.vernemq.com/plugin-development/introduction)

Next image from official documentation about [plugin chaining](https://docs.vernemq.com/plugin-development/introduction#chaining):

[![plugin chaining](dev/chaining.png)](dev/chaining.png)

There is some of VerneMQ hooks we use in current example:
- [auth_on_register](https://docs.vernemq.com/plugin-development/sessionlifecycle#auth_on_register-and-auth_on_register_m5)
hook used by:
    - [current plugin](vmq_auth_chx/src/vmq_auth_chx.erl)
    - [vmq_passwd plugin](https://github.com/vernemq/vernemq/blob/master/apps/vmq_passwd/src/vmq_passwd.erl)
- [auth_on_subscribe](https://docs.vernemq.com/plugin-development/subscribeflow#auth_on_subscribe-and-auth_on_subscribe_m5)
and [auth_on_publish](https://docs.vernemq.com/plugin-development/publishflow#auth_on_publish-and-auth_on_publish_m5)
hooks used by:
    - only [vmq_acl plugin](https://github.com/vernemq/vernemq/blob/master/apps/vmq_acl/src/vmq_acl.erl)

`auth_on_register` chain: `current_plugin -> vmq_passwd`

`auth_on_subscribe` chain: only `vmq_acl`

Why our plugin goes first?
Some info about plugin priority from default `vernemq.conf` file:

>plugins.\<name\>.priority defines the load order of the
>plugins. Plugins are loaded by priority. If no priority is given
>the load order is undefined. Prioritized plugins will always be
>loaded before plugins with no defined priority.
>
>Acceptable values:
>  - an integer
>
>plugins.mypluginname.priority = 5

In example above (if you use `./restart.sh`) current plugin starts with [priority = 5](restart.sh#L7),
because the environment variable
> `DOCKER_VERNEMQ_plugins__vmq_auth_chx__priority="5"`

will become as next line in `vernemq.conf` configuration file:
>  plugins.vmq_auth_chx.priority = 5

So, our plugin is prioritized and that is why it
>will always be loaded before plugins with no defined priority


## Next steps: customization

Set users. In current example there is two users defined in [`vmq.passwd`](vmq.passwd):
- `test` with password `test`
- `nifi` with password `nifi`

Setup your control list. ACL for current example defined at [`vmq.acl`](vmq.acl)

Official documentation about auth ([here](https://docs.vernemq.com/configuring-vernemq/file-auth)).

:thumbsup:

---
---
---

###### next session - default README.md from TuMaTu
 
---
---
---

# VerneMQ Auth Plugin

Documentation by Seth Moeckel

This is an Erlang based plugin for authenticating MQTT clients with JWT within the VerneMQ MQTT Broker. In addition to authentication on client connection, this plugin provides access control mechanisms for publish/subscribe topic authorization.

### Startup

**Erlang, Docker and Docker-Compose must be installed in order to run this project. Additionally, Fetching the secret from Vault will fail unless connected to York VPN (Clients won't be able to connect due to auth failures)**

Compile the plugin by navigating to `cd vmq_auth_chx` and running `rebar3 compile`.

Run the docker container 
`docker-compose up -d`
(updates to the plugin code will require `--build` flag)

**Code away....**

If the auth plugin is changed, Open a container TTY (I use Kitematic)
and use `bash app/reload.sh` to remove the old plugin/enable the new.

Beware!!! Bad code can cause VerneMQ to crash (non fatal)...

### Packaging the plugin

Given you have run `rebar3 compile` within `vmq_auth_chx`, you may then run `bash ../package-plugin.sh` to
clean up the compiled plugin located in `_build/default` and create a tar archive from it.  The generated tar archive will be `vmq_auth_chx/vmq_auth_chx_plugin.tar.gz`.


### Project Structure

Everything related to the plugin code is within the `vmq_auth_chx` directory. Plugin source code is within the `vmq_auth_chx/src`.

#### Plugin Development

Since this plugin is Erlang based, directory setup and tooling may seem foreign. The `rebar3` binary is located in the repository and is used to manage dependencies and compile the plugin. A list of dependencies is managed within `vmq_auth_chx/rebar.config` which are fetched when the plugin is first compiled. In order to compile the plugin, `rebar3 compile` must be run from within `vmq_auth_chx`. The resulting plugin will be located in `vmq_auth_chx/_build/default`.

The primary file related to the plugin and the module containing the broker hooks is `vmq_auth_chx/src/vmq_auth_chx.erl`. Application startup information and hooks used are within ```vmq_auth_chx/src/vmq_auth_chx.app.src``` whereas supervisor and application start methods are within the ```vmq_auth_chx/src/vmq_auth_chx_app.erl ``` and ```vmq_auth_chx/src/vmq_auth_chx_sup.erl```.



### Docker/Broker Configuration

This project is based off of the docker `erlio/docker-vernemq` image. All broker config is set via environment variables in the docker-compose file. Specifc variable and configuration information can be found on the VerneMQ doc (below). 

Since this plugin needs to access Vault over HTTPS, be sure to enable ports 80 and 443 in the docker-compose file.



### Links & Resources

**Hackney HTTP Library**: https://github.com/benoitc/hackney

**Erl-JWT JWT Library**:https://github.com/marianoguerra/jwt-erl

**JSX Erlang JSON Library**:https://github.com/talentdeficit/jsx



**JWT Generation Tool**:http://jwtbuilder.jamiekurtz.com/

**Vault HTTP API Docs**:https://www.vaultproject.io/docs/index.html

**VerneMQ Plugin**:

​   https://github.com/erlio/vernemq_demo_plugin

​   https://vernemq.com/docs/plugindevelopment/

​   https://vernemq.com/docs/configuration/

​   

**VerneMQ Slack Team**:https://vernemq.com/community.html

**Erlang Slack Team**:https://erlanger.slack.com/messages

​   

### Vault integration

The following Curl requests are the identical to those made by this plugin within the `fetchSecret()` function.

Get Vault Token

```http
curl -X POST \
-d '{"role_id":"vmq-auth-chx-crosschxdev","secret_id":"<secret id>"}' \
<approle url> | jq .

```

Get Vault Secret

```http
curl \
-H "X-Vault-Token: <token here>" \
-X GET \
<access url> | jq .
```

The forementioned token, access/approle url and secret id are provided as OS environment variables in the docker compose file.



# docker-vernemq

## How to use this image

### Start a VerneMQ cluster node

    docker run --name vernemq1 -d erlio/docker-vernemq
   
Somtimes you need to configure a forwarding for ports (on a Mac for example):

    docker run -p 1883:1883 --name vernemq1 -d erlio/docker-vernemq

This starts a new node that listens on 1883 for MQTT connections and on 8080 for MQTT over websocket connections. However, at this moment the broker won't be able to authenticate the connecting clients. To allow anonymous clients use the ```DOCKER_VERNEMQ_ALLOW_ANONYMOUS=on``` environment variable.

    docker run -e "DOCKER_VERNEMQ_ALLOW_ANONYMOUS=on" --name vernemq1 -d erlio/docker-vernemq
    
### Autojoining a VerneMQ cluster

This allows a newly started container to automatically join a VerneMQ cluster. Assuming you started your first node like the example above you could autojoin the cluster (which currently consists of a single container 'vernemq1') like the following:

    docker run -e "DOCKER_VERNEMQ_DISCOVERY_NODE=<IP-OF-VERNEMQ1>" --name vernemq2 -d erlio/docker-vernemq

(Note, you can find the IP of a docker container using `docker inspect <containername/cid> | grep \"IPAddress\"`).

### Checking cluster status

To check if the bove containers have successfully clustered you can issue the ```vmq-admin``` command:

    docker exec vernemq1 vmq-admin cluster status
    +--------------------+-------+
    |        Node        |Running|
    +--------------------+-------+
    |VerneMQ@172.17.0.151| true  |
    |VerneMQ@172.17.0.152| true  |
    +--------------------+-------+
    
All ```vmq-admin``` commands are available. See https://vernemq.com/docs/administration/ for more information.

### VerneMQ Configuration

All configuration parameters that are available in `vernemq.conf` can be defined
using the `DOCKER_VERNEMQ` prefix followed by the confguration parameter name.
E.g: `allow_anonymous=on` is `-e "DOCKER_VERNEMQ_ALLOW_ANONYMOUS=on"` or
`allow_register_during_netsplit=on` is
`-e "DOCKER_VERNEMQ_ALLOW_REGISTER_DURING_NETSPLIT=on"`. All available configuration
parameters can be found on https://vernemq.com/docs/configuration/.

#### Remarks

Some of our configuration variables contain dots `.`. For example if you want to
adjust the log level of VerneMQ you'd use `-e
"DOCKER_VERNEMQ_LOG.CONSOLE.LEVEL=debug"`. However, some container platforms
such as Kubernetes don't support dots and other special characters in 
environment variables. If you are on such a platform you could substitute the
dots with two underscores `__`. The example above would look like `-e
"DOCKER_VERNEMQ_LOG__CONSOLE__LEVEL=debug"`.