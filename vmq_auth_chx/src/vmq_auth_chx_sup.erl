-module(vmq_auth_chx_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).
-define(Version, "1.0.0").

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
  error_logger:info_msg("STARTING VMQ_AUTH_CHX PLUGIN VERSION: ~p ", [?Version]),

  % Lets create an Erlang ets table for storing the vault secret.
  error_logger:info_msg("CREATING ERLANG ETS TABLE STORAGE: \"storage\""),
  ets:new(storage, [public, named_table]),

  supervisor:start_link({local, ?MODULE}, ?MODULE, []).
%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
  {ok, {{one_for_one, 5, 10}, []}}.

