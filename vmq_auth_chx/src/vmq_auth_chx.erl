-module(vmq_auth_chx).

-behaviour(auth_on_register_hook).

-export([auth_on_register/5]).

% constant named LogOut set to the value of an OS env var, 
% used to determine if something should be outputted or not.
-define(LogOut, os:getenv("VMQ_AUTH_CHX_LOG_STDOUT", "yes")).

% constant set to the get_vsecret() function. Can be called as
% ?Secret, Returns the secret by way of ETS Table or Vault HTTP
% request.
-define(Secret, get_vsecret()).

% this record defines the jwt schema used by the erl-jwt library.
-record(jwt, {exp, nbf, iat, iss, aud, prn, jti, typ, enc, body, alg, sig, actual_sig}).

% This file contains the methods used by VerneMQ to Authenticate 
% users and control pub/sub access to topics. There are three  
% specific hooks utilized directly by VerneMQ that are fired  
% from client connection/pub/sub events: auth_on_register,
% auth_on_publish and auth_on_subscribe. Each hook contains
% the parameters of the requests themselves and only expects
% a status to be returned.



fetchSecret() ->
  % This function is used to create a Vault User Token and retrieve a
  % secret from Vault using said token. The token is then stored in ETS
  % (Erlang Term Storage - Memory Persistance)
  % and used for validating all user-provided JWT in the auth_on_register
  % hook below.

  % These are OS env variables used to specify credentials for use with Vault.
  % {ENVVAR} = os:getenv(<ENV NAME>, <DEFAULT TO IF !EXISTS>)
  Secret = os:getenv("VMQ_AUTH_CHX_SECRET"),

  if
    Secret /= false ->


      % % if logging is enabled, log out registration params.
      case ?LogOut of
        "yes" ->
          error_logger:info_msg("Secret: ~p ", [Secret]);
        _ -> ok
      end,

      % return secret...
      ets:insert(storage, {<<"secretValue">>, Secret}),
      ok;

    true ->
      case ?LogOut of
        "yes" ->
          error_logger:info_msg("MISSING ENV VARIABLES, PLEASE SET VARS AND RELOAD PLUGIN. DUMP: Secret: ~p ", [Secret]);

        _ -> ok
      end
  end.



get_vsecret() ->
  % This function simply is called by hook methods
  % for every new connection. It then performs an ets
  % lookup, fetches the secret if necessary and
  % returns the secret to the hook.
  case ets:lookup(storage, <<"secretValue">>) of
    [{_Name, Secret}] ->
      Secret;
    [] ->
      fetchSecret(),
      {_Name, Secret} = hd(ets:lookup(storage, <<"secretValue">>)),
      Secret
  end.



auth_on_register({_IpAddr, _Port} = Peer, {_MountPoint, _ClientId} = SubscriberId, UserName, Password, CleanSession) ->

  % This function is used to validate client information on connection.

  % if logging is enabled, log out registration params.
  case ?LogOut of
    "yes" ->
      error_logger:info_msg("auth_on_register: ~p ~p ~p ~p", [Peer, SubscriberId, UserName, CleanSession]);
    _ -> ok
  end,

  %% do whatever you like with the params, all that matters
  %% is the return value of this function
  %%
  %% 1. return 'ok' -> CONNECT is authenticated
  %% 2. return 'next' -> leave it to other plugins to decide
  %% 3. return {ok, [{ModifierKey, NewVal}...]} -> CONNECT is authenticated, but we might want to set some options used throughout the client session:
  %%      - {mountpoint, NewMountPoint::string}
  %%      - {clean_session, NewCleanSession::boolean}
  %% 4. return {error, invalid_credentials} -> CONNACK_CREDENTIALS is sent
  %% 5. return {error, whatever} -> CONNACK_AUTH is sent

  {_, Client} = SubscriberId,

  case UserName of
    % if the username is "JWT"
    <<"JWT">> ->
      % Decode and validate the JWT Token
      {Valid, Decoded} = jwt:decode(Password, ?Secret),
      Body = jsx:decode(Decoded#jwt.body, [return_maps]),
      {ok, UserId} = maps:find(<<"userId">>, Body),
      error_logger:info_msg("Valid: ~p ; Client: ~p ; UserId: ~p", [Valid, Client, UserId]),

      if
      % if the token is valid, the clientId
      % matches the userId (UUID) provided
      % within the JWT claims, return ok.
        Valid == ok, Client == UserId -> ok;

      % else, return invalid creds.
        true ->
          {error, invalid_credentials}
      end;

    % otherwise, return invalid creds.
    _ ->
      next
%%            {error, invalid_credentials}

    % close the clause and return the status of connection.
  end.
